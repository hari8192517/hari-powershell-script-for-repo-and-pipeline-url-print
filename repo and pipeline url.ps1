$personalAccessToken = "glpat-8f5_7Ls3xTwQr5sgNzz_"

# Function to list repositories and check pipeline status
function Get-RepositoriesAndCheckPipelines {
    param (
        [string]$groupId,
        [string]$personalAccessToken
    )

    $baseUrl = "https://gitlab.com/api/v4"
    $headers = @{
        "PRIVATE-TOKEN" = $personalAccessToken
    }

    # Get list of projects in the group
    $url = "$baseUrl/groups/$groupId/projects"
    $projects = Invoke-RestMethod -Uri $url -Method Get -Headers $headers

    foreach ($project in $projects) {
        $projectId = $project.id
        $projectName = $project.name
        $url = "$baseUrl/projects/$projectId/pipelines"

        # Get pipelines for the project
        $pipelines = Invoke-RestMethod -Uri $url -Method Get -Headers $headers

        foreach ($pipeline in $pipelines) {
            $pipelineId = $pipeline.id
            $pipelineUrl = "https://gitlab.com/$($project.path_with_namespace)/-/pipelines/$pipelineId"

            # Get pipeline details
            $url = "$baseUrl/projects/$projectId/pipelines/$pipelineId"
            $pipelineDetails = Invoke-RestMethod -Uri $url -Method Get -Headers $headers

            # Check if all jobs in stage "npe_deploy" are in completed state
            $jobsUrl = "$baseUrl/projects/$projectId/pipelines/$pipelineId/jobs"
            $jobs = Invoke-RestMethod -Uri $jobsUrl -Method Get -Headers $headers

            $npeDeployJobs = $jobs | Where-Object { $_.stage -eq "npe_deploy" }
            $incompleteJobs = $npeDeployJobs | Where-Object { $_.status -ne "success" }

            # Print repo name and URL only if all jobs in "npe_deploy" stage are completed
            if (-not $incompleteJobs) {
                Write-Output ("Repository: {0}, Pipeline URL: {1}" -f $projectName, $pipelineUrl)
            }
        }
    }
}

# Prompt the user to enter the group ID
$groupId = Read-Host "Enter the group ID"

# Call the function
Get-RepositoriesAndCheckPipelines -groupId $groupId -personalAccessToken $personalAccessToken | Out-File -FilePath "pipeline_output.txt"
